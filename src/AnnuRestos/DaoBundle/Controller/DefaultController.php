<?php

namespace AnnuRestos\DaoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('DaoBundle:Default:index.html.twig', array('name' => $name));
    }
}
