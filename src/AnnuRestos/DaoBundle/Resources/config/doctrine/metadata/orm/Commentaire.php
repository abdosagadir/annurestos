<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Commentaire
 *
 * @ORM\Table(name="commentaire", indexes={@ORM\Index(name="I_FK_COMMENTAIRE_UTILISATEUR", columns={"IDUTILISATEUR"}), @ORM\Index(name="I_FK_COMMENTAIRE_ARTICLE", columns={"IDARTICLE"}), @ORM\Index(name="I_FK_COMMENTAIRE_UTILISATEUR1", columns={"IDUTILISATEUR_ADMIN"})})
 * @ORM\Entity
 */
class Commentaire
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IDCOMMENTAIRE", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcommentaire;

    /**
     * @var string
     *
     * @ORM\Column(name="CONTENNU", type="string", length=255, nullable=false)
     */
    private $contennu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DHCOMMENTAIRE", type="datetime", nullable=false)
     */
    private $dhcommentaire;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IDUTILISATEUR", referencedColumnName="IDUTILISATEUR")
     * })
     */
    private $idutilisateur;

    /**
     * @var \Article
     *
     * @ORM\ManyToOne(targetEntity="Article")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IDARTICLE", referencedColumnName="IDARTICLE")
     * })
     */
    private $idarticle;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IDUTILISATEUR_ADMIN", referencedColumnName="IDUTILISATEUR")
     * })
     */
    private $idutilisateurAdmin;


}
