<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Typejours
 *
 * @ORM\Table(name="typejours")
 * @ORM\Entity
 */
class Typejours
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IDTYPEJOURS", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtypejours;

    /**
     * @var string
     *
     * @ORM\Column(name="LIBELLETYPEJOURS", type="string", length=50, nullable=false)
     */
    private $libelletypejours;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Restaurant", inversedBy="idtypejours")
     * @ORM\JoinTable(name="ouvre",
     *   joinColumns={
     *     @ORM\JoinColumn(name="IDTYPEJOURS", referencedColumnName="IDTYPEJOURS")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="IDRESTAURANT", referencedColumnName="IDRESTAURANT")
     *   }
     * )
     */
    private $idrestaurant;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idrestaurant = new \Doctrine\Common\Collections\ArrayCollection();
    }

}
