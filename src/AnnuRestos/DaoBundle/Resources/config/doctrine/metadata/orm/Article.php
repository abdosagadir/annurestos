<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Article
 *
 * @ORM\Table(name="article", indexes={@ORM\Index(name="I_FK_ARTICLE_UTILISATEUR", columns={"IDUTILISATEUR_ADMIN"})})
 * @ORM\Entity
 */
class Article
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IDARTICLE", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idarticle;

    /**
     * @var string
     *
     * @ORM\Column(name="TITRE", type="string", length=128, nullable=false)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="CONTENNU", type="string", length=255, nullable=false)
     */
    private $contennu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DHARTICLE", type="datetime", nullable=false)
     */
    private $dharticle;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IDUTILISATEUR_ADMIN", referencedColumnName="IDUTILISATEUR")
     * })
     */
    private $idutilisateurAdmin;


}
