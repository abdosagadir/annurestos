<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Previlege
 *
 * @ORM\Table(name="previlege")
 * @ORM\Entity
 */
class Previlege
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IDPRIVILEGE", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idprivilege;

    /**
     * @var string
     *
     * @ORM\Column(name="TYPEPREVILEGE", type="string", length=128, nullable=false)
     */
    private $typeprevilege;


}
