<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Categorie
 *
 * @ORM\Table(name="categorie")
 * @ORM\Entity
 */
class Categorie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IDCATEGORIER", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcategorier;

    /**
     * @var string
     *
     * @ORM\Column(name="LIBELLECATEGORIE", type="string", length=128, nullable=false)
     */
    private $libellecategorie;


}
