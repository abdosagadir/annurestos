<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Restaurant
 *
 * @ORM\Table(name="restaurant", indexes={@ORM\Index(name="I_FK_RESTAURANT_ARTICLE", columns={"IDARTICLE"}), @ORM\Index(name="I_FK_RESTAURANT_CATEGORIE", columns={"IDCATEGORIER"}), @ORM\Index(name="I_FK_RESTAURANT_VILLE", columns={"IDVILE"})})
 * @ORM\Entity
 */
class Restaurant
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IDRESTAURANT", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idrestaurant;

    /**
     * @var string
     *
     * @ORM\Column(name="NOMRESTAURANT", type="string", length=128, nullable=false)
     */
    private $nomrestaurant;

    /**
     * @var string
     *
     * @ORM\Column(name="ADRESSE", type="string", length=128, nullable=false)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="DESCRIPTION", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="PRIX", type="float", precision=5, scale=2, nullable=true)
     */
    private $prix;

    /**
     * @var boolean
     *
     * @ORM\Column(name="PARKING", type="boolean", nullable=true)
     */
    private $parking;

    /**
     * @var boolean
     *
     * @ORM\Column(name="CARTECREDIT", type="boolean", nullable=true)
     */
    private $cartecredit;

    /**
     * @var \Article
     *
     * @ORM\ManyToOne(targetEntity="Article")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IDARTICLE", referencedColumnName="IDARTICLE")
     * })
     */
    private $idarticle;

    /**
     * @var \Categorie
     *
     * @ORM\ManyToOne(targetEntity="Categorie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IDCATEGORIER", referencedColumnName="IDCATEGORIER")
     * })
     */
    private $idcategorier;

    /**
     * @var \Ville
     *
     * @ORM\ManyToOne(targetEntity="Ville")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IDVILE", referencedColumnName="IDVILE")
     * })
     */
    private $idvile;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Typejours", mappedBy="idrestaurant")
     */
    private $idtypejours;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idtypejours = new \Doctrine\Common\Collections\ArrayCollection();
    }

}
