<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Utilisateur
 *
 * @ORM\Table(name="utilisateur", indexes={@ORM\Index(name="I_FK_UTILISATEUR_PREVILEGE", columns={"IDPRIVILEGE"})})
 * @ORM\Entity
 */
class Utilisateur
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IDUTILISATEUR", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idutilisateur;

    /**
     * @var string
     *
     * @ORM\Column(name="NOM", type="string", length=128, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="PRENOM", type="string", length=128, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="LOGIN", type="string", length=128, nullable=false)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="MOTDEPASSE", type="string", length=128, nullable=false)
     */
    private $motdepasse;

    /**
     * @var string
     *
     * @ORM\Column(name="MAIL", type="string", length=128, nullable=false)
     */
    private $mail;

    /**
     * @var \Previlege
     *
     * @ORM\ManyToOne(targetEntity="Previlege")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IDPRIVILEGE", referencedColumnName="IDPRIVILEGE")
     * })
     */
    private $idprivilege;


}
