<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Avis
 *
 * @ORM\Table(name="avis", indexes={@ORM\Index(name="I_FK_AVIS_RESTAURANT", columns={"IDRESTAURANT"}), @ORM\Index(name="I_FK_AVIS_UTILISATEUR", columns={"IDUTILISATEUR"})})
 * @ORM\Entity
 */
class Avis
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IDAVIS", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idavis;

    /**
     * @var float
     *
     * @ORM\Column(name="NOTE", type="float", precision=2, scale=2, nullable=false)
     */
    private $note;

    /**
     * @var \Restaurant
     *
     * @ORM\ManyToOne(targetEntity="Restaurant")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IDRESTAURANT", referencedColumnName="IDRESTAURANT")
     * })
     */
    private $idrestaurant;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IDUTILISATEUR", referencedColumnName="IDUTILISATEUR")
     * })
     */
    private $idutilisateur;


}
