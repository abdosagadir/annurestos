<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * Ville
 *
 * @ORM\Table(name="ville")
 * @ORM\Entity
 */
class Ville
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IDVILE", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idvile;

    /**
     * @var string
     *
     * @ORM\Column(name="NOMVILLE", type="string", length=128, nullable=true)
     */
    private $nomville;


}
