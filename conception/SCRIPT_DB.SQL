-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Dim 27 Avril 2014 à 15:39
-- Version du serveur: 5.5.20
-- Version de PHP: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `annurestosdb`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `IDARTICLE` int(11) NOT NULL AUTO_INCREMENT,
  `IDUTILISATEUR_ADMIN` int(11) DEFAULT NULL,
  `TITRE` varchar(128) NOT NULL,
  `CONTENNU` varchar(255) NOT NULL,
  `DHARTICLE` datetime NOT NULL,
  PRIMARY KEY (`IDARTICLE`),
  KEY `I_FK_ARTICLE_UTILISATEUR` (`IDUTILISATEUR_ADMIN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `avis`
--

CREATE TABLE IF NOT EXISTS `avis` (
  `IDAVIS` int(11) NOT NULL AUTO_INCREMENT,
  `IDRESTAURANT` int(11) DEFAULT NULL,
  `IDUTILISATEUR` int(11) DEFAULT NULL,
  `NOTE` double(2,2) NOT NULL,
  PRIMARY KEY (`IDAVIS`),
  KEY `I_FK_AVIS_RESTAURANT` (`IDRESTAURANT`),
  KEY `I_FK_AVIS_UTILISATEUR` (`IDUTILISATEUR`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `IDCATEGORIER` int(11) NOT NULL AUTO_INCREMENT,
  `LIBELLECATEGORIE` varchar(128) NOT NULL,
  PRIMARY KEY (`IDCATEGORIER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

CREATE TABLE IF NOT EXISTS `commentaire` (
  `IDCOMMENTAIRE` int(11) NOT NULL AUTO_INCREMENT,
  `IDUTILISATEUR` int(11) DEFAULT NULL,
  `IDARTICLE` int(11) DEFAULT NULL,
  `IDUTILISATEUR_ADMIN` int(11) DEFAULT NULL,
  `CONTENNU` varchar(255) NOT NULL,
  `DHCOMMENTAIRE` datetime NOT NULL,
  PRIMARY KEY (`IDCOMMENTAIRE`),
  KEY `I_FK_COMMENTAIRE_UTILISATEUR` (`IDUTILISATEUR`),
  KEY `I_FK_COMMENTAIRE_ARTICLE` (`IDARTICLE`),
  KEY `I_FK_COMMENTAIRE_UTILISATEUR1` (`IDUTILISATEUR_ADMIN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `ouvre`
--

CREATE TABLE IF NOT EXISTS `ouvre` (
  `IDTYPEJOURS` int(11) NOT NULL AUTO_INCREMENT,
  `IDRESTAURANT` int(11) NOT NULL,
  `HEUREOVERTURE` time NOT NULL,
  `HEUREFERMETURE` time NOT NULL,
  PRIMARY KEY (`IDTYPEJOURS`,`IDRESTAURANT`),
  KEY `I_FK_OUVRE_TYPEJOURS` (`IDTYPEJOURS`),
  KEY `I_FK_OUVRE_RESTAURANT` (`IDRESTAURANT`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `previlege`
--

CREATE TABLE IF NOT EXISTS `previlege` (
  `IDPRIVILEGE` int(11) NOT NULL AUTO_INCREMENT,
  `TYPEPREVILEGE` varchar(128) NOT NULL,
  PRIMARY KEY (`IDPRIVILEGE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `restaurant`
--

CREATE TABLE IF NOT EXISTS `restaurant` (
  `IDRESTAURANT` int(11) NOT NULL AUTO_INCREMENT,
  `IDARTICLE` int(11) NOT NULL,
  `IDCATEGORIER` int(11) NOT NULL,
  `IDVILE` int(11) NOT NULL,
  `NOMRESTAURANT` varchar(128) NOT NULL,
  `ADRESSE` varchar(128) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `PRIX` double(5,2) DEFAULT NULL,
  `PARKING` tinyint(1) DEFAULT NULL,
  `CARTECREDIT` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`IDRESTAURANT`),
  KEY `I_FK_RESTAURANT_ARTICLE` (`IDARTICLE`),
  KEY `I_FK_RESTAURANT_CATEGORIE` (`IDCATEGORIER`),
  KEY `I_FK_RESTAURANT_VILLE` (`IDVILE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `typejours`
--

CREATE TABLE IF NOT EXISTS `typejours` (
  `IDTYPEJOURS` int(11) NOT NULL AUTO_INCREMENT,
  `LIBELLETYPEJOURS` varchar(50) NOT NULL,
  PRIMARY KEY (`IDTYPEJOURS`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `IDUTILISATEUR` int(11) NOT NULL AUTO_INCREMENT,
  `IDPRIVILEGE` int(11) NOT NULL,
  `NOM` varchar(128) DEFAULT NULL,
  `PRENOM` varchar(128) DEFAULT NULL,
  `LOGIN` varchar(128) NOT NULL,
  `MOTDEPASSE` varchar(128) NOT NULL,
  `MAIL` varchar(128) NOT NULL,
  PRIMARY KEY (`IDUTILISATEUR`),
  KEY `I_FK_UTILISATEUR_PREVILEGE` (`IDPRIVILEGE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

CREATE TABLE IF NOT EXISTS `ville` (
  `IDVILE` int(11) NOT NULL AUTO_INCREMENT,
  `NOMVILLE` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`IDVILE`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`IDUTILISATEUR_ADMIN`) REFERENCES `utilisateur` (`IDUTILISATEUR`);

--
-- Contraintes pour la table `avis`
--
ALTER TABLE `avis`
  ADD CONSTRAINT `avis_ibfk_1` FOREIGN KEY (`IDRESTAURANT`) REFERENCES `restaurant` (`IDRESTAURANT`),
  ADD CONSTRAINT `avis_ibfk_2` FOREIGN KEY (`IDUTILISATEUR`) REFERENCES `utilisateur` (`IDUTILISATEUR`);

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `commentaire_ibfk_1` FOREIGN KEY (`IDUTILISATEUR`) REFERENCES `utilisateur` (`IDUTILISATEUR`),
  ADD CONSTRAINT `commentaire_ibfk_2` FOREIGN KEY (`IDARTICLE`) REFERENCES `article` (`IDARTICLE`),
  ADD CONSTRAINT `commentaire_ibfk_3` FOREIGN KEY (`IDUTILISATEUR_ADMIN`) REFERENCES `utilisateur` (`IDUTILISATEUR`);

--
-- Contraintes pour la table `ouvre`
--
ALTER TABLE `ouvre`
  ADD CONSTRAINT `ouvre_ibfk_1` FOREIGN KEY (`IDTYPEJOURS`) REFERENCES `typejours` (`IDTYPEJOURS`),
  ADD CONSTRAINT `ouvre_ibfk_2` FOREIGN KEY (`IDRESTAURANT`) REFERENCES `restaurant` (`IDRESTAURANT`);

--
-- Contraintes pour la table `restaurant`
--
ALTER TABLE `restaurant`
  ADD CONSTRAINT `restaurant_ibfk_1` FOREIGN KEY (`IDARTICLE`) REFERENCES `article` (`IDARTICLE`),
  ADD CONSTRAINT `restaurant_ibfk_2` FOREIGN KEY (`IDCATEGORIER`) REFERENCES `categorie` (`IDCATEGORIER`),
  ADD CONSTRAINT `restaurant_ibfk_3` FOREIGN KEY (`IDVILE`) REFERENCES `ville` (`IDVILE`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `utilisateur_ibfk_1` FOREIGN KEY (`IDPRIVILEGE`) REFERENCES `previlege` (`IDPRIVILEGE`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
